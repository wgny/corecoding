// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.net;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.EOFException;
import java.net.Socket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ConnectException;
import javax.net.SocketFactory;
import java.util.concurrent.*;

import com.brianwolter.coding.*;
import com.brianwolter.coding.io.*;

/**
 * A message socket
 */
public class MessageSocket<M> {
  
  private volatile SocketFactory            _socketFactory;
  private volatile DecodeInputStream<M>     _inputStream;
  private volatile EncodeOutputStream<M>    _outputStream;
  private LinkedBlockingQueue<M>            _outputQueue;
  private volatile MessageSocketDelegate<M> _delegate;
  private volatile boolean                  _disconnecting;
  
  private Socket                            _socket;
  private DecodeBuffer<M>                   _decoder;
  private EncodeBuffer<M>                   _encoder;
  private Input                             _input;
  private Output                            _output;
  private InetSocketAddress                 _address;
  
  /**
   * Construct
   */
  public MessageSocket(SocketFactory socketFactory, DecodeBuffer<M> decoder, EncodeBuffer<M> encoder, MessageSocketDelegate<M> delegate) throws Exception {
    this(socketFactory, 512, decoder, encoder, delegate);
  }
  
  /**
   * Construct
   */
  public MessageSocket(SocketFactory socketFactory, int capacity, DecodeBuffer<M> decoder, EncodeBuffer<M> encoder, MessageSocketDelegate<M> delegate) throws Exception {
    _outputQueue = new LinkedBlockingQueue<M>(capacity);
    _socketFactory = socketFactory;
    _decoder = decoder;
    _encoder = encoder;
    _delegate = delegate;
  }
  
  /**
   * Determine if this socket is connected
   */
  public boolean isConnected() {
    synchronized(this){
      return _socket != null;
    }
  }
  
  /**
   * Connect this socket to its remote endpoint
   */
  public void connect(String host, int port) {
    connect(new InetSocketAddress(host, port));
  }
  
  /**
   * Connect this socket to its remote endpoint
   */
  public void connect(InetSocketAddress address) {
    synchronized(this){
      if(_input == null){
        _address = address;
        _input = new Input();
        _input.setDaemon(true);
        _input.start();
      }
    }
  }
  
  /**
   * Disconnect this socket
   */
  public void disconnect() {
    if(_disconnecting == false){
      _disconnecting = true;
      synchronized(this){
        
        try {
          
          // clean up after the input thread if it's still running
          if(_input != null && _input.isRunning()){
            _input.interrupt();
            _input.join();
          }
          
          // clean up after the output thread if it's still running
          if(_output != null && _output.isRunning()){
            _output.interrupt();
            _output.join();
          }
          
          // clear our input thread
          _input = null;
          // clear our output thread
          _output = null;
          // clear our socket
          _socket = null;
          // clear our input stream
          _inputStream = null;
          // clear our output stream
          _outputStream = null;
          
          // notify our delegate we've disconnected
          if(_delegate != null){
            _delegate.messageSocketDidDisconnect(this);
          }
          
        }catch(InterruptedException e){
          System.err.println("Unable to disconnect message socket: "+ e.getMessage());
          e.printStackTrace();
        }
        
      }
      _disconnecting = false;
    }
  }
  
  /**
   * Send a message to the remote host
   */
  public boolean send(M message) {
    synchronized(this){
      if(_output != null){
        return _output.send(message);
      }else{
        return false;
      }
    }
  }
  
  /**
   * String description
   */
  public String toString() {
    synchronized(this){
      return "<MessageSocket "+ ((_socket != null) ? _socket.getRemoteSocketAddress() : "(not connected)") +">";
    }
  }
  
  /**
   * Input worker
   */
  private class Input extends Thread {
    
    private boolean _running;
    
    /**
     * Determine if this thread is running or not
     */
    public synchronized boolean isRunning() {
      return _running;
    }
    
    /**
     * We were disconnected
     */
    private synchronized void disconnected() {
      // we're not running anymore
      _running = false;
      // force a disconnect
      disconnect();
    }
    
    /**
     * Entry
     */
    public void run() {
      try {
        
        // we're running now
        boolean running;
        synchronized(this){ _running = running = true; }
        
        // we'll attempt to connect
        if(_delegate != null) _delegate.messageSocketWillConnect(MessageSocket.this);
        
        // setup our socket and connect
        _socket = _socketFactory.createSocket(_address.getAddress(), _address.getPort());
        // setup our input stream
        _inputStream = new DecodeInputStream<M>(_socket.getInputStream(), _decoder);
        // setup our input stream
        _outputStream = new EncodeOutputStream<M>(_socket.getOutputStream(), _encoder);
        
        // setup our output thread
        _output = new Output();
        _output.setDaemon(true);
        _output.start();
        
        // we did connect
        if(_delegate != null) _delegate.messageSocketDidConnect(MessageSocket.this);
        
        // read messages until we run out; the stream should throw an EOF exception when it's finished
        while(running){
          M message;
          while(_running && (message = _inputStream.readMessage()) != null){
            if(_delegate != null){ _delegate.messageSocketReceivedMessage(MessageSocket.this, message); }
          }
          synchronized(this){ running = _running; };
        }
        
      }catch(ConnectException e){
        // no need to report this, just disconnect
      }catch(EOFException e){
        // no need to report this, just disconnect
      }catch(Exception e){
        if(_delegate != null) _delegate.messageSocketError(MessageSocket.this, e);
      }finally{
        disconnected();
      }
    }
  }
  
  /**
   * Output worker
   */
  private class Output extends Thread {
    
    private boolean _running;
    
    /**
     * Send a message
     */
    public boolean send(M message) {
      return _outputQueue.offer(message);
    }
    
    /**
     * Determine if this thread is running or not
     */
    public synchronized boolean isRunning() {
      return _running;
    }
    
    /**
     * We were disconnected
     */
    private synchronized void disconnected() {
      // we're not running anymore
      _running = false;
      // force a disconnect
      disconnect();
    }
    
    /**
     * Entry
     */
    public void run() {
      try {
        
        // we're running now
        boolean running;
        synchronized(this){ _running = running = true; }
        
        // process messages forever
        while(running){
          M message;
          if((message = _outputQueue.poll(10, TimeUnit.SECONDS)) != null){ _outputStream.writeMessage(message); }
          synchronized(this){ running = _running; };
        }
        
      }catch(InterruptedException e){
        // no need to report this, just disconnect
      }catch(Exception e){
        if(_delegate != null) _delegate.messageSocketError(MessageSocket.this, e);
      }finally{
        disconnected();
      }
    }
    
  }
  
}

