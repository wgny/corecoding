// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.message;

import com.brianwolter.coding.CodingException;

/**
 * Base message exception.
 * 
 * @author Brian William Wolter
 */
public class MessageException extends CodingException {
  
  /**
   * Construct with no information
   */
  public MessageException(){
    super();
  }
  
  /**
   * Construct with a message describing the exception.
   * 
   * @param m the message
   */
  public MessageException(String m){
    super(m);
  }
  
  /**
   * Construct with a message describing the exception and a root exception from
   * which this originated.
   * 
   * @param m the message
   * @param e the root exception
   */
  public MessageException(String m, Throwable e){
    super(m, e);
  }
  
  /**
   * Construct with a root exception from which this exception originated.
   * 
   * @param e the root exception
   */
  public MessageException(Throwable e){
    super(e);
  }
  
}




