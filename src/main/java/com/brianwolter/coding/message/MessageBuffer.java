// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.message;

import java.nio.ByteBuffer;
import java.util.*;

/**
 * A message data buffer.
 */
public class MessageBuffer {
  
  /* Hex characters */
  protected static final char[] HEX_CHARS = {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
  };
  
  /* Buffer resize factor */
  private static final int RESIZE_FACTOR    = 2;
  
  /* Public types */
  public static final byte TYPE_NULL        = 0;
  public static final byte TYPE_BOOLEAN     = 1;
  public static final byte TYPE_INTEGER     = 2;
  public static final byte TYPE_DOUBLE      = 3;
  public static final byte TYPE_STRING      = 4;
  public static final byte TYPE_BYTES       = 5;
  public static final byte TYPE_ARRAY       = 6;
  public static final byte TYPE_DICTIONARY  = 7;
  
  /* Unknown type placeholder (error type) */
  public static final byte TYPE_UNKNOWN     = 127;
  
  /* Internal boolean types */
  public static final byte TYPE_TRUE        = 8;
  public static final byte TYPE_FALSE       = 9;
  
  /* Internal integer types */
  public static final byte TYPE_SINT8       = 10;
  public static final byte TYPE_SINT16      = 11;
  public static final byte TYPE_SINT24      = 12;
  public static final byte TYPE_SINT32      = 13;
  public static final byte TYPE_SINT40      = 14;
  public static final byte TYPE_SINT48      = 15;
  public static final byte TYPE_SINT56      = 16;
  public static final byte TYPE_SINT64      = 17;
  
  /* Internal integer constant types */
  public static final byte TYPE_ZERO        = 18;
  public static final byte TYPE_ONE         = 19;
  
  /* Internal floating point types */
  public static final byte TYPE_FLOAT32     = 20;
  public static final byte TYPE_FLOAT64     = 21;
  
  protected ByteBuffer  _buffer;
  protected int         _offset;
  protected int         _length;
  
  /**
   * Determine if the provided value is a message primitive
   */
  public boolean isValueSupported(Object value) {
    return value == null || isTypeSupported(value.getClass());
  }
  
  /**
   * Determine if the provided type is a message primitive
   */
  public boolean isTypeSupported(Class clazz) {
    return
      Boolean.class.isAssignableFrom(clazz)     ||
      boolean.class.isAssignableFrom(clazz)     ||
      Short.class.isAssignableFrom(clazz)       ||
      short.class.isAssignableFrom(clazz)       ||
      Integer.class.isAssignableFrom(clazz)     ||
      int.class.isAssignableFrom(clazz)         ||
      Long.class.isAssignableFrom(clazz)        ||
      long.class.isAssignableFrom(clazz)        ||
      Float.class.isAssignableFrom(clazz)       ||
      float.class.isAssignableFrom(clazz)       ||
      Double.class.isAssignableFrom(clazz)      ||
      double.class.isAssignableFrom(clazz)      ||
      String.class.isAssignableFrom(clazz)      ||
      byte[].class.isAssignableFrom(clazz)      ||
      Collection.class.isAssignableFrom(clazz)  ||
      Map.class.isAssignableFrom(clazz);
  }
  
  /**
   * Obtain the length of the buffer
   */
  public int length() {
    return _length;
  }
  
  /**
   * Obtain buffered bytes
   */
  public byte[] bytes() {
    byte[] buffer = _buffer.array();
    byte[] actual = new byte[_length];
    System.arraycopy(buffer, 0, actual, 0, _length);
    return actual;
  }
  
  /**
   * Obtain buffered bytes
   */
  public ByteBuffer buffer() {
    return (ByteBuffer)_buffer.duplicate().flip();
  }
  
  /**
   * Get the internal buffer position
   */
  public int position() {
    return _buffer.position();
  }
  
  /**
   * Set the internal buffer position
   */
  public void position(int position) {
    _buffer.position(position);
  }
  
  /**
   * Require capacity in the message buffer
   */
  protected void requireCapacity(int capacity) {
    if(_buffer.capacity() < capacity){
      capacity = Math.max(capacity, _buffer.capacity() * RESIZE_FACTOR);
      int position = _buffer.position();
      _buffer = ByteBuffer.allocate(capacity).put((ByteBuffer)_buffer.flip());
      _buffer.position(position);
    }
  }
  
  /**
   * Determine the number of bytes required to represent the provided value
   */
  protected int minBytesForInteger(long value) {
    long uv = (value < 0) ? -value : value;
    int bits; for(bits = 0; uv > 0; bits++) uv = uv >>> 1;
    return (int)Math.ceil((double)(bits + 1) / 8.0d);
  }
  
  /**
   * Encode integer bytes for the specified value
   */
  protected byte[] encodeIntegerBytes(long value) {
    return new byte[] {
      (byte)((value >>> 56) & 0xffL),
      (byte)((value >>> 48) & 0xffL),
      (byte)((value >>> 40) & 0xffL),
      (byte)((value >>> 32) & 0xffL),
      (byte)((value >>> 24) & 0xffL),
      (byte)((value >>> 16) & 0xffL),
      (byte)((value >>>  8) & 0xffL),
      (byte)((value >>>  0) & 0xffL)
    };
  }
  
  /**
   * Decode an integer from the provided integer bytes
   */
  protected long decodeIntegerBytes(byte[] bytes) {
    if(bytes.length > 8) throw new IllegalArgumentException("Byte buffer exceeds the maximum precision of an integer value");
    long value = 0L; // start with zero
    for(int i = 0; i < bytes.length; i++){
      // the most significant byte allows a sign extension in the OR operation; lower bytes do not
      value |= ((i == 0) ? bytes[i] : (bytes[i] & 0xffL)) << ((bytes.length - i - 1) * 8);
    }
    return value;
  }
  
  /**
   * String description
   */
  public static String bytesToString(byte[] content) {
    return bytesToString(content, 0, content.length);
  }
  
  /**
   * String description
   */
  public static String bytesToString(byte[] content, int offset, int length) {
    StringBuffer sb = new StringBuffer();
    sb.append("<");
    
    for(int i = 0; i < length; i++){
      if(i > 0 && (i % 4) == 0) sb.append(" ");
      sb.append(HEX_CHARS[(content[offset + i] >>> 4) & 0x0f]);
      sb.append(HEX_CHARS[(content[offset + i] >>> 0) & 0x0f]);
    }
    
    sb.append(">");
    return sb.toString();
  }
  
  /**
   * String description
   */
  public String toString() {
    return bytesToString(_buffer.array(), _offset, _length);
  }
  
}


