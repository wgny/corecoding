// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.message;

import java.nio.ByteBuffer;
import java.util.*;

import com.brianwolter.coding.*;

/**
 * Message encoder.
 */
public class MessageEncoder extends MessageBuffer implements Encoder {
  
  /**
   * Construct a message buffer
   */
  public MessageEncoder() {
    _buffer = ByteBuffer.allocate(64);
    _buffer.rewind();
    _length = 0;
  }
  
  /**
   * Encode an object. The object must be a supported property type.
   */
  public void encode(Object value) throws MessageException {
    if(value == null){
      encodeNull();
    }else if(Boolean.class.isAssignableFrom(value.getClass())){
      encodeBoolean((Boolean)value);
    }else if(Short.class.isAssignableFrom(value.getClass()) || Integer.class.isAssignableFrom(value.getClass()) || Long.class.isAssignableFrom(value.getClass())){
      encodeInteger(((Number)value).longValue());
    }else if(Float.class.isAssignableFrom(value.getClass()) || Double.class.isAssignableFrom(value.getClass())){
      encodeDouble(((Number)value).doubleValue());
    }else if(String.class.isAssignableFrom(value.getClass())){
      encodeString((String)value);
    }else if(byte[].class.isAssignableFrom(value.getClass())){
      encodeData((byte[])value);
    }else if(Collection.class.isAssignableFrom(value.getClass())){
      encodeArrayCollection((Collection)value);
    }else if(Map.class.isAssignableFrom(value.getClass())){
      encodeDictionaryCollection((Map)value);
    }else{
      throw new IllegalArgumentException("Type is not supported: "+ value.getClass());
    }
  }
  
  /**
   * Encode an array collection. The collection must be a supported property type.
   */
  protected void encodeArrayCollection(Collection value) throws MessageException {
    encodeArray(value.size());
    for(Object element : value){
      encode(element);
    }
  }
  
  /**
   * Encode a dictionary collection. The collection must be a supported property type.
   */
  protected void encodeDictionaryCollection(Map value) throws MessageException {
    encodeDictionary(value.size());
    for(Object key : value.keySet()){
      encode(key); encode(value.get(key));
    }
  }
  
  /**
   * Encode a null value
   */
  public void encodeNull() {
    requireCapacity(_buffer.position() + 1);
    _buffer.put(TYPE_NULL);
    _length = _buffer.position();
  }
  
  /**
   * Encode a boolean value
   */
  public void encodeBoolean(boolean value) {
    requireCapacity(_buffer.position() + 1);
    _buffer.put((value) ? TYPE_TRUE : TYPE_FALSE);
    _length = _buffer.position();
  }
  
  /**
   * Encode a integer value
   */
  public void encodeInteger(long value) {
    int bytes = minBytesForInteger(value);
    requireCapacity(_buffer.position() + 1 + bytes);
    _buffer.put((byte)(TYPE_SINT8 + bytes - 1));
    _buffer.put(encodeIntegerBytes(value), ((Long.SIZE / 8) - bytes), bytes);
    _length = _buffer.position();
  }
  
  /**
   * Encode a double value
   */
  public void encodeDouble(double value) {
    requireCapacity(_buffer.position() + 1 + (Double.SIZE / 8));
    _buffer.put(TYPE_DOUBLE);
    _buffer.putDouble(value);
    _length = _buffer.position();
  }
  
  /**
   * Encode a string value
   */
  public void encodeString(String value) {
    byte[] utf8 = encodeStringBytes(value);
    requireCapacity(_buffer.position() + 1 + (Integer.SIZE / 8) + utf8.length);
    _buffer.put(TYPE_STRING);
    encodeInteger(utf8.length);
    _buffer.put(utf8, 0, utf8.length);
    _length = _buffer.position();
  }
  
  /**
   * Encode string bytes
   */
  private byte[] encodeStringBytes(String value) {
    try {
      return value.getBytes("UTF-8");
    }catch(java.io.UnsupportedEncodingException e) {
      throw new IllegalStateException("UTF-8 string encoding is not supported (this is preposterous)");
    }
  }
  
  /**
   * Encode a byte array value
   */
  public void encodeData(byte[] bytes) {
    requireCapacity(_buffer.position() + 1 + (Integer.SIZE / 8) + bytes.length);
    _buffer.put(TYPE_BYTES);
    encodeInteger(bytes.length);
    _buffer.put(bytes, 0, bytes.length);
    _length = _buffer.position();
  }
  
  /**
   * Encode an array value
   */
  public void encodeArray(int count) {
    requireCapacity(_buffer.position() + 1 + (Integer.SIZE / 8));
    _buffer.put(TYPE_ARRAY);
    encodeInteger(count);
    _length = _buffer.position();
  }
  
  /**
   * Encode an dictionary value
   */
  public void encodeDictionary(int count) {
    requireCapacity(_buffer.position() + 1 + (Integer.SIZE / 8));
    _buffer.put(TYPE_DICTIONARY);
    encodeInteger(count);
    _length = _buffer.position();
  }
  
}


