// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.message;

import java.nio.ByteBuffer;
import java.util.*;

import com.brianwolter.coding.*;

/**
 * Message buffer. Note that the JVM is big-endian as is the message buffer
 * interchange format, so we do not concern ourselves with byte order.
 */
public class MessageDecoder extends MessageBuffer implements Decoder, Iterable<Object> {
  
  /**
   * Construct a message buffer
   */
  public MessageDecoder(byte[] bytes) {
    this(bytes, 0, bytes.length);
  }
  
  /**
   * Construct a message buffer
   */
  public MessageDecoder(byte[] bytes, int location, int length) {
    _buffer = (ByteBuffer)ByteBuffer.wrap(bytes, location, length).rewind();
    _length = length;
  }
  
  /**
   * Construct a message buffer.
   */
  public MessageDecoder(ByteBuffer buffer) {
    _buffer = buffer;
    _offset = buffer.position();
    _length = buffer.remaining();
  }
  
  /**
   * Obtain an object iterator. Note that the iterator affects the state of this decoder.
   */
  public Iterator<Object> iterator() {
    return new MessagePropertyIterator<Object>((ByteBuffer)_buffer.duplicate().rewind());
  }
  
  /**
   * Determine if more properties are available
   */
  public boolean hasProperties() {
    return _buffer.remaining() > 0;
  }
  
  /**
   * Decode the Java type of the next value in the decoder, or null the next value is of type
   * null. Note that this operation does not advance the decoder cursor or swallow the type
   * indicator byte. A subsequent call of decode[Type]() will work as expected.
   */
  public Class decodeType() throws MessageException {
    requireAvailableBytes(1 /* type code */);
    byte type = _buffer.get(_buffer.position());
    switch(type){
      case TYPE_NULL:
        return null;
      case TYPE_TRUE: case TYPE_FALSE:
        return Boolean.class;
      case TYPE_SINT8: case TYPE_SINT16: case TYPE_SINT24: case TYPE_SINT32: case TYPE_SINT40: case TYPE_SINT48: case TYPE_SINT56: case TYPE_SINT64:
        return Number.class;
      case TYPE_DOUBLE:
        return Number.class;
      case TYPE_STRING:
        return String.class;
      case TYPE_BYTES:
        return byte[].class;
      case TYPE_ARRAY:
        return List.class;
      case TYPE_DICTIONARY:
        return Map.class;
      default:
        throw new MessageException("Type is not supported ["+ type +"] at "+ _buffer.position() +" in: "+ this);
    }
  }
  
  /**
   * Decode an object
   */
  public Object decode() throws MessageException {
    requireAvailableBytes(1 /* type code */);
    byte type = _buffer.get(_buffer.position());
    switch(type){
      case TYPE_NULL:
        return decodeNull();
      case TYPE_TRUE: case TYPE_FALSE:
        return decodeBoolean();
      case TYPE_SINT8: case TYPE_SINT16: case TYPE_SINT24: case TYPE_SINT32: case TYPE_SINT40: case TYPE_SINT48: case TYPE_SINT56: case TYPE_SINT64:
        return decodeInteger();
      case TYPE_DOUBLE:
        return decodeDouble();
      case TYPE_STRING:
        return decodeString();
      case TYPE_BYTES:
        return decodeData();
      case TYPE_ARRAY:
        return decodeArrayCollection();
      case TYPE_DICTIONARY:
        return decodeDictionaryCollection();
      default:
        throw new MessageException("Type is not supported ["+ type +"] at "+ _buffer.position() +" in: "+ this);
    }
  }
  
  /**
   * Decode an array collection
   */
  protected List<Object> decodeArrayCollection() throws MessageException {
    List<Object> array = new ArrayList<Object>();
    int count = decodeArray();
    for(int i = 0; i < count; i++) array.add(decode());
    return array;
  }
  
  /**
   * Decode a dictionary collection
   */
  protected Map<Object, Object> decodeDictionaryCollection() throws MessageException {
    Map<Object, Object> dict = new HashMap<Object, Object>();
    int count = decodeDictionary();
    for(int i = 0; i < count; i++) dict.put(decode(), decode());
    return dict;
  }
  
  /**
   * Decode a null value (swallow the type byte and return null)
   */
  public Object decodeNull() throws MessageException {
    requireAvailableBytes(1 /* type code */);
    byte type = _buffer.get();
    if(type != TYPE_NULL) throw new MessageException("Property is not a null");
    return null;
  }
  
  /**
   * Decode a boolean value
   */
  public boolean decodeBoolean() throws MessageException {
    requireAvailableBytes(1 /* type code */);
    byte type = _buffer.get();
    if(type != TYPE_TRUE && type != TYPE_FALSE) throw new MessageException("Property is not a boolean");
    return (type == TYPE_TRUE) ? true : false;
  }
  
  /**
   * Decode an integer value
   */
  public long decodeInteger() throws MessageException {
    requireAvailableBytes(1 /* type code */);
    byte type = _buffer.get();
    if(type < TYPE_SINT8 || type > TYPE_SINT64) throw new MessageException("Property is not an integer");
    int bytes = type - TYPE_SINT8 + 1;
    requireAvailableBytes(bytes);
    byte[] buffer = new byte[bytes];
    _buffer.get(buffer, 0, bytes);
    return decodeIntegerBytes(buffer);
  }
  
  /**
   * Decode a double value
   */
  public double decodeDouble() throws MessageException {
    requireAvailableBytes(1 + (Double.SIZE / 8));
    byte type = _buffer.get();
    if(type != TYPE_DOUBLE) throw new MessageException("Property is not a double");
    return _buffer.getDouble();
  }
  
  /**
   * Decode a double value
   */
  public String decodeString() throws MessageException {
    requireAvailableBytes(1 /* type code */);
    
    byte type = _buffer.get();
    if(type != TYPE_STRING) throw new MessageException("Property is not a string");
    long length = decodeInteger();
    if(length > Integer.MAX_VALUE) throw new MessageException("String length exceeds maximum possible value");
    
    requireAvailableBytes(length);
    byte[] bytes = new byte[(int)length];
    _buffer.get(bytes);
    
    return decodeStringBytes(bytes);
  }
  
  /**
   * Decode string bytes
   */
  private String decodeStringBytes(byte[] bytes) {
    try {
      return new String(bytes, "UTF-8");
    }catch(java.io.UnsupportedEncodingException e) {
      throw new IllegalStateException("UTF-8 string encoding is not supported (this is preposterous)");
    }
  }
  
  /**
   * Decode a byte array value
   */
  public byte[] decodeData() throws MessageException {
    requireAvailableBytes(1 /* type code */);
    
    byte type = _buffer.get();
    if(type != TYPE_BYTES) throw new MessageException("Property is not a byte array");
    long length = decodeInteger();
    if(length > Integer.MAX_VALUE) throw new MessageException("Data length exceeds maximum possible value");
    
    requireAvailableBytes(length);
    byte[] bytes = new byte[(int)length];
    _buffer.get(bytes);
    
    return bytes;
  }
  
  /**
   * Decode an array value
   */
  public int decodeArray() throws MessageException {
    requireAvailableBytes(1 /* type code */);
    byte type = _buffer.get();
    if(type != TYPE_ARRAY) throw new MessageException("Property is not an array");
    long length = decodeInteger();
    if(length > Integer.MAX_VALUE) throw new MessageException("Array count exceeds maximum possible value");
    return (int)length;
  }
  
  /**
   * Decode an integer value
   */
  public int decodeDictionary() throws MessageException {
    requireAvailableBytes(1 /* type code */);
    byte type = _buffer.get();
    if(type != TYPE_DICTIONARY) throw new MessageException("Property is not an dictionary");
    long length = decodeInteger();
    if(length > Integer.MAX_VALUE) throw new MessageException("Dictionary count exceeds maximum possible value");
    return (int)length;
  }
  
  /**
   * Require the buffer contain some number of bytes
   */
  private void requireAvailableBytes(long required) throws MessageException {
    if(_buffer.remaining() < required) throw new MessageException("Not enough bytes remain in the buffer to extract a value (position: "+ _buffer.position() +", remaining: "+ _buffer.remaining() +", required: "+ required +")");
  }
  
  /**
   * String description
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("<MessageDecoder");
    sb.append("\n");
    
    int i = 0;
    for(Object object : this){
      sb.append("  #"+ (i++) +": ");
      if(object == null) sb.append("<null>");
      else sb.append("("+ object.getClass().getName() +") "+ object);
      sb.append("\n");
    }
    
    sb.append(">");
    return sb.toString();
  }
  
}

/**
 * A message property iterator
 */
class MessagePropertyIterator<T> implements Iterator<T> {
  
  // internal decoder so we don't change the original decoder's state
  private MessageDecoder _decoder;
  
  /**
   * Construct with message data.
   */
  public MessagePropertyIterator(ByteBuffer buffer) {
    _decoder = new MessageDecoder(buffer);
  }
  
  /**
   * Determine if we have more properties
   */
  public boolean hasNext() {
    return _decoder.hasProperties();
  }
  
  /**
   * Obtain the next property
   */
  public T next() {
    try { return (T)_decoder.decode(); }catch(Exception e){
      throw new IllegalStateException("Unable to decode element", e);
    }
  }
  
  /**
   * Not supported
   */
  public void remove() {
    throw new UnsupportedOperationException("Remove message property via iterator is not supported");
  }
  
}


