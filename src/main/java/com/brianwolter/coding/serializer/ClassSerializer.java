// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.serializer;

import java.util.Date;
import java.lang.reflect.Array;

import com.brianwolter.coding.*;

/**
 * A class serializer. Note that this serializer requires that that a decoded class
 * actually be available to the decoding JVM.
 * 
 * Primitives, classes, and single-dimension arrays of primitives or classes are
 * supported by this serializer. Multi-dimension arrays, parameterized types, and
 * other more complex types are not fully supported.
 */
public class ClassSerializer implements Serializer {
  
  private static final String ARRAY_SUFFIX = "[]";
  
  /**
   * Transform an object to its external representation and add it to the provided encoder
   */
  public void encode(Encoder encoder, Object object) throws Exception {
    if(object == null){
      encoder.encodeNull();
    }else if(Class.class.isAssignableFrom(object.getClass())){
      encoder.encodeString(((Class)object).getCanonicalName());
    }else{
      throw new CodingException(getClass().getName() +" only supports object of type: "+ Class.class.getName());
    }
  }
  
  /**
   * Transform data from the provided decoder into its internal (object) form
   */
  public Object decode(Decoder decoder) throws Exception {
    Object value = decoder.decode();
    if(value == null){
      return null;
    }else if(String.class.isAssignableFrom(value.getClass())){
      String string = (String)value;
      if(string.equals("byte")){
        return byte.class;
      }else if(string.equals("byte"+ ARRAY_SUFFIX)){
        return byte[].class;
      }else if(string.equals("boolean")){
        return boolean.class;
      }else if(string.equals("boolean"+ ARRAY_SUFFIX)){
        return boolean[].class;
      }else if(string.equals("short")){
        return short.class;
      }else if(string.equals("short"+ ARRAY_SUFFIX)){
        return short[].class;
      }else if(string.equals("int")){
        return int.class;
      }else if(string.equals("int"+ ARRAY_SUFFIX)){
        return int[].class;
      }else if(string.equals("long")){
        return long.class;
      }else if(string.equals("long"+ ARRAY_SUFFIX)){
        return long[].class;
      }else if(string.equals("float")){
        return float.class;
      }else if(string.equals("float"+ ARRAY_SUFFIX)){
        return float[].class;
      }else if(string.equals("double")){
        return double.class;
      }else if(string.equals("double"+ ARRAY_SUFFIX)){
        return double[].class;
      }else if(string.endsWith(ARRAY_SUFFIX)){
        return Array.newInstance(Class.forName(string.substring(0, string.length() - ARRAY_SUFFIX.length())), 0).getClass();
      }else{
        return Class.forName(string);
      }
    }else{
      throw new CodingException("Could not decode "+ Class.class.getName() +" from: "+ value);
    }
  }
  
}


