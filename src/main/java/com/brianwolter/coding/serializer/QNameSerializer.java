// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.serializer;

import com.brianwolter.core.util.QName;

import com.brianwolter.coding.*;

/**
 * A qualified name serializer
 */
public class QNameSerializer implements Serializer {
  
  /**
   * Transform an object to its external representation and add it to the provided encoder
   */
  public void encode(Encoder encoder, Object object) throws Exception {
    if(object == null){
      encoder.encodeNull();
    }else if(QName.class.isAssignableFrom(object.getClass())){
      encoder.encodeString(((QName)object).getName());
    }else{
      throw new CodingException(getClass().getName() +" only supports objects of type "+ QName.class.getName());
    }
  }
  
  /**
   * Transform data from the provided decoder into its internal (object) form
   */
  public Object decode(Decoder decoder) throws Exception {
    Object name;
    if((name = decoder.decode()) == null){
      return null;
    }else if(String.class.isAssignableFrom(name.getClass())){
      return new QName((String)name);
    }else{
      throw new CodingException(getClass().getName() +" expected qualified names to be encoded as "+ String.class.getName());
    }
  }
  
}


