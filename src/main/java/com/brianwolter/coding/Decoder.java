// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding;

/**
 * Implemented by serializing decoders.
 */
public interface Decoder extends Iterable<Object> {
  
  /**
   * Determine if the provided object is a supported property type.
   */
  public boolean isValueSupported(Object value);
  
  /**
   * Determine if the provided class is a supported property type.
   */
  public boolean isTypeSupported(Class clazz);
  
  /**
   * Decode the type of the next object, without advancing the cursor.
   */
  public Class decodeType() throws CodingException;
  
  /**
   * Decode the next object
   */
  public Object decode() throws CodingException;
  
  /**
   * Decode a null value
   */
  public Object decodeNull() throws CodingException;
  
  /**
   * Decode a boolean value
   */
  public boolean decodeBoolean() throws CodingException;
  
  /**
   * Decode an integer value
   */
  public long decodeInteger() throws CodingException;
  
  /**
   * Decode a double value
   */
  public double decodeDouble() throws CodingException;
  
  /**
   * Decode a double value
   */
  public String decodeString() throws CodingException;
  
  /**
   * Decode a byte array value
   */
  public byte[] decodeData() throws CodingException;
  
  /**
   * Decode an array value
   */
  public int decodeArray() throws CodingException;
  
  /**
   * Decode an integer value
   */
  public int decodeDictionary() throws CodingException;
  
}


