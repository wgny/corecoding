// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.annotation;

import java.lang.annotation.*;

import com.brianwolter.coding.Serializer;
import com.brianwolter.coding.Validator;

/**
 * Decorates a serializable property field
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Property {
  
  /**
   * The index at which this property is serialized in a message buffer.
   */
  int index() default -1;
  
  /**
   * The concrete class to use for the container in an array/collection type. If missing, a reasonable default is used. This property takes precedence.
   */
  Class array() default java.lang.Object.class;
  
  /**
   * The class to use for elements in a collection type. If missing, the type either must be declared as a generic or an element serializer must be defined. This property takes precedence.
   */
  Class elements() default java.lang.Object.class;
  
  /**
   * The serializer to use for encoding and decoding elements in a collection type. If missing, the type must be declared as a generic. This property takes precedence.
   */
  Class<? extends Serializer> elementSerializer() default com.brianwolter.coding.serializer.DefaultSerializer.class;
  
  /**
   * The concrete class to use for the container in a dictionary type. If missing, a reasonable default is used. This property takes precedence.
   */
  Class dictionary() default java.lang.Object.class;
  
  /**
   * The class to use for keys in a dictionary type. If missing, the type must be declared as a generic. This property takes precedence.
   */
  Class keys() default java.lang.Object.class;
  
  /**
   * The class to use for values in a dictionary type. If missing, the type must be declared as a generic. This property takes precedence.
   */
  Class values() default java.lang.Object.class;
  
  /**
   * The serializer to use for encoding and decoding keys in a dictionary type. If missing, the type must be declared as a generic. This property takes precedence.
   */
  Class<? extends Serializer> keySerializer() default com.brianwolter.coding.serializer.DefaultSerializer.class;
  
  /**
   * The serializer to use for encoding and decoding values in a dictionary type. If missing, the type must be declared as a generic. This property takes precedence.
   */
  Class<? extends Serializer> valueSerializer() default com.brianwolter.coding.serializer.DefaultSerializer.class;
  
  /**
   * The serializer class to use for encoding or decoding values.
   */
  Class<? extends Serializer> serializer() default com.brianwolter.coding.serializer.DefaultSerializer.class;
  
  /**
   * Validator classes to apply in declaration order to the property value when decoding it.
   */
  Class<? extends Validator>[] validators() default { };
  
}

