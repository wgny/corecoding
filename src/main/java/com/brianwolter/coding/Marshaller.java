// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding;

import java.lang.reflect.*;
import java.lang.annotation.*;
import java.util.*;
import java.beans.*;

import com.brianwolter.coding.annotation.*;
import com.brianwolter.coding.serializer.DefaultSerializer;

/**
 * A serialized package marshaller
 */
public class Marshaller {
  
  private static final Map<Class<? extends Serializer>, Serializer> _serializers  = new HashMap<Class<? extends Serializer>, Serializer>();
  private static final Map<Class<? extends Validator>, Validator>   _validators   = new HashMap<Class<? extends Validator>, Validator>();
  private static final Object _lock = new Object();
  
  /**
   * Obtain a serializer of the specified type. If a serializer instance already exists,
   * it is returned. Otherwise, an instance is created, cached, and that is returned.
   */
  protected static Serializer getSerializerInstance(Class<? extends Serializer> clazz) throws CodingException {
    try {
      // if the serializer class is null, return null
      if(clazz == null) return null;
      // otherwise produce a serializer
      synchronized(_lock){
        Serializer serializer;
        if((serializer = _serializers.get(clazz)) == null) _serializers.put(clazz, serializer = clazz.newInstance());
        return serializer;
      }
    }catch(Exception e){
      throw new CodingException("Unable to obtain or create serializer instance: "+ clazz, e);
    }
  }
  
  /**
   * Obtain a validator of the specified type. If a validator instance already exists,
   * it is returned. Otherwise, an instance is created, cached, and that is returned.
   */
  protected static Validator getValidatorInstance(Class<? extends Validator> clazz) throws CodingException {
    try {
      // if the serializer class is null, return null
      if(clazz == null) return null;
      // otherwise produce a serializer
      synchronized(_lock){
        Validator validator;
        if((validator = _validators.get(clazz)) == null) _validators.put(clazz, validator = clazz.newInstance());
        return validator;
      }
    }catch(Exception e){
      throw new CodingException("Unable to obtain or create validator instance: "+ clazz, e);
    }
  }
  
  /**
   * Marshall an object into the provided message encoder.
   */
  public <E extends Encoder> E marshall(Object object, E encoder) throws CodingException {
    try {
      return (E)marshallObject(object, encoder);
    }catch(CodingException e){
      throw e;
    }catch(Exception e){
      throw new CodingException("Unable to marshall object of type: "+ object.getClass().getName(), e);
    }
  }
  
  /**
   * Marshall an object into the provided message encoder.
   */
  protected Encoder marshallObject(Object object, Encoder encoder) throws CodingException {
    if(Coding.class.isAssignableFrom(object.getClass())){
      return marshallObjectProperties(object, getPropertiesForObject(object), encoder);
    }else if(Encodable.class.isAssignableFrom(object.getClass())){
      return ((Encodable)object).encode(encoder);
    }else{
      throw new CodingException("Marshalled objects must implement "+ Coding.class.getName() +": "+ object.getClass().getName());
    }
  }
  
  /**
   * Marshall object properties into the provided message encoder.
   */
  protected Encoder marshallObjectProperties(Object object, List<Field> fields, Encoder encoder) throws CodingException {
    for(Field field : fields) marshallObjectProperty(object, field, encoder);
    return encoder;
  }
  
  /**
   * Marshall object properties into the provided message encoder.
   */
  protected Encoder marshallObjectProperty(Object object, Field field, Encoder encoder) throws CodingException {
    boolean accessible;
    Object value;
    
    try {
      if(!(accessible = field.isAccessible())) field.setAccessible(true);
    }catch(SecurityException e){
      throw new CodingException("Unable to make field of marshalled object accessible: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    Class type;
    if((type = field.getType()) == null){
      throw new CodingException("Marshalled object field strangely has no type: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    Property annotation;
    if((annotation = field.getAnnotation(Property.class)) == null){
      throw new CodingException("No property annotation found on marshalled object field: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    try {
      value = field.get(object);
    }catch(Exception e){
      throw new CodingException("Marshalled object field could not be accessed: "+ object.getClass().getName() +"."+ field.getName(), e);
    }
    
    Class[] validators;
    if((validators = annotation.validators()) != null && validators.length > 0){
      for(int i = 0; i < validators.length; i++){
        getValidatorInstance(validators[i]).validate(object, field, value);
      }
    }
    
    marshallValue(object, value, encoder, annotation);
    
    try {
      if(field.isAccessible() != accessible) field.setAccessible(accessible);
    }catch(SecurityException e){
      throw new CodingException("Unable to restore field accessibility of marshalled object: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    return encoder;
  }
  
  /**
   * Marshall a value
   */
  protected void marshallValue(Object object, Object value, Encoder encoder, Property annotation) throws CodingException {
    Class<? extends Serializer> serializer;
    if((serializer = annotation.serializer()) != null && serializer != DefaultSerializer.class){
      encode(value, encoder, serializer);
    }else if(value == null){
      encoder.encodeNull();
    }else if(Coding.class.isAssignableFrom(value.getClass())){
      marshallObject(value, encoder);
    }else if(Encodable.class.isAssignableFrom(value.getClass())){
      marshallObject(value, encoder);
    }else if(Collection.class.isAssignableFrom(value.getClass())){
      marshallArrayCollection(object, (Collection)value, encoder, annotation);
    }else if(Map.class.isAssignableFrom(value.getClass())){
      marshallDictionaryCollection(object, (Map)value, encoder, annotation);
    }else{
      encode(value, encoder, serializer);
    }
  }
  
  /**
   * Marshall an array collection
   */
  protected void marshallArrayCollection(Object object, Collection value, Encoder encoder, Property annotation) throws CodingException {
    Class<? extends Serializer> serializer = annotation.elementSerializer();
    encoder.encodeArray(value.size());
    for(Object element : value){
      if(serializer != null && serializer != DefaultSerializer.class){
        encode(value, encoder, serializer);
      }else{
        marshallValue(object, element, encoder, annotation);
      }
    }
  }
  
  /**
   * Marshall a dictionary collection
   */
  protected void marshallDictionaryCollection(Object object, Map value, Encoder encoder, Property annotation) throws CodingException {
    encoder.encodeDictionary(value.size());
    for(Map.Entry entry : (Set<Map.Entry>)value.entrySet()){
      marshallValue(object, entry.getKey(), encoder, annotation); marshallValue(object, entry.getValue(), encoder, annotation);
    }
  }
  
  /**
   * Unmarshall a message decoder into an object of the provided type.
   */
  public <T> T unmarshall(Decoder decoder, Class<T> clazz) throws CodingException {
    try {
      return (T)unmarshallObject(decoder, clazz);
    }catch(CodingException e){
      throw e;
    }catch(Exception e){
      throw new CodingException("Unable to unmarshall object of type: "+ clazz.getName(), e);
    }
  }
  
  /**
   * Unmarshall a message decoder into the provided object.
   */
  protected Object unmarshallObject(Decoder decoder, Class clazz) throws CodingException {
    return unmarshallObject(decoder, newInstance(clazz));
  }
  
  /**
   * Unmarshall a message decoder into the provided object.
   */
  protected Object unmarshallObject(Decoder decoder, Object object) throws CodingException {
    if(Coding.class.isAssignableFrom(object.getClass())){
      return unmarshallObjectProperties(decoder, object, getPropertiesForObject(object));
    }else if(Decodable.class.isAssignableFrom(object.getClass())){
      return ((Decodable)object).decode(decoder);
    }else{
      throw new CodingException("Unmarshalled objects must implement "+ Coding.class.getName() +": "+ object.getClass().getName());
    }
  }
  
  /**
   * Unmarshall a message decoder into the provided object properties
   */
  protected Object unmarshallObjectProperties(Decoder decoder, Object object, List<Field> fields) throws CodingException {
    for(Field field : fields) unmarshallObjectProperty(decoder, object, field);
    return object;
  }
  
  /**
   * Unmarshall a message decoder into the provided object property
   */
  protected Object unmarshallObjectProperty(Decoder decoder, Object object, Field field) throws CodingException {
    boolean accessible;
    Object value;
    
    try {
      if(!(accessible = field.isAccessible())) field.setAccessible(true);
    }catch(SecurityException e){
      throw new CodingException("Unable to make field of unmarshalled object accessible: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    Type type;
    if((type = field.getGenericType()) == null){
      throw new CodingException("Unmarshalled object field strangely has no generic type: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    Class base;
    if((base = field.getType()) == null){
      throw new CodingException("Unmarshalled object field strangely has no base type: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    Property annotation;
    if((annotation = field.getAnnotation(Property.class)) == null){
      throw new CodingException("No property annotation found on marshalled object field: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    try {
      value = unmarshallValue(decoder, object, base, type, field, annotation);
    }catch(Exception e){
      throw new CodingException("Unable to decode value for unmarshalled object field: "+ object.getClass().getName() +"."+ field.getName(), e);
    }
    
    try {
      field.set(object, value);
    }catch(Exception e){
      throw new CodingException("Unable to access unmarshalled object field: "+ object.getClass().getName() +"."+ field.getName(), e);
    }
    
    try {
      if(field.isAccessible() != accessible) field.setAccessible(accessible);
    }catch(SecurityException e){
      throw new CodingException("Unable to restore field accessibility of unmarshalled object: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    return object;
  }
  
  /**
   * Unmarshall a value
   */
  protected Object unmarshallValue(Decoder decoder, Object object, Class base, Type generic, Field field, Property annotation) throws CodingException {
    Object value = null;
    
    Class<? extends Serializer> serializer;
    if((serializer = annotation.serializer()) != null && serializer != DefaultSerializer.class){
      value = decode(base, decoder, serializer);
    }else if(Coding.class.isAssignableFrom(base)){
      value = unmarshallObject(decoder, base);
    }else if(Decodable.class.isAssignableFrom(base)){
      value = unmarshallObject(decoder, base);
    }else if(Collection.class.isAssignableFrom(base)){
      value = unmarshallArrayCollection(decoder, object, field, generic, base, annotation);
    }else if(Map.class.isAssignableFrom(base)){
      value = unmarshallDictionaryCollection(decoder, object, field, generic, base, annotation);
    }else{
      value = decode(base, decoder, serializer);
    }
    
    Class<? extends Validator>[] validators;
    if((validators = annotation.validators()) != null && validators.length > 0){
      for(int i = 0; i < validators.length; i++){
        getValidatorInstance(validators[i]).validate(object, field, value);
      }
    }
    
    return value;
  }
  
  /**
   * Unmarshall an array collection
   */
  protected Collection unmarshallArrayCollection(Decoder decoder, Object object, Field field, Type generic, Class base, Property annotation) throws CodingException {
    Class collection = null;
    Class element = null;
    Class declared = null;
    Collection array;
    
    int count;
    if(decoder.decodeType() == null){
      return (Collection)decoder.decodeNull();
    }else{
      count = decoder.decodeArray();
    }
    
    if((declared = annotation.array()) != null && !declared.equals(java.lang.Object.class)){
      collection = declared;
    }else if(base.isInterface()){
      if(Set.class.isAssignableFrom(base)){
        collection = HashSet.class;
      }else if(List.class.isAssignableFrom(base)){
        collection = ArrayList.class;
      }else if(Queue.class.isAssignableFrom(base)){
        collection = LinkedList.class;
      }else if(Collection.class.isAssignableFrom(base)){
        collection = ArrayList.class;
      }else{
        throw new CodingException("Could not derive a suitable concrete type for abstract or interface field type "+ base.getName() +" for unmarshalled object field: "+ object.getClass().getName() +"."+ field.getName());
      }
    }else{
      collection = base;
    }
    
    Type[] vtypes = (generic instanceof ParameterizedType) ? ((ParameterizedType)generic).getActualTypeArguments() : null;
    
    if((declared = annotation.elements()) != null && !java.lang.Object.class.equals(declared)){
      element = declared;
    }else if(vtypes != null && vtypes.length == 1){
      element = getBaseType(vtypes[0]);
    }else{
      throw new CodingException("Collection property declares no concrete element type for unmarshalled object field: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    try {
      array = (Collection)newInstance(collection);
    }catch(Exception e){
      throw new CodingException("Unable to instantiate collection of type "+ collection.getName() +"<"+ element.getName() +"> for unmarshalled field: "+ object.getClass().getName() +"."+ field.getName(), e);
    }
    
    Class<? extends Serializer> serializer = annotation.elementSerializer();
    int index = 0;
    
    try {
      for(index = 0; index < count; index++){
        if(serializer != null && serializer != DefaultSerializer.class){
          array.add(decode(element, decoder, serializer));
        }else{
          array.add(unmarshallValue(decoder, object, element, (vtypes != null && vtypes.length > 0) ? vtypes[0] : null, field, annotation));
        }
      }
    }catch(Exception e){
      throw new CodingException("Unable to decode element at index "+ index +" for collection of type "+ collection.getName() +"<"+ element.getName() +"> for unmarshalled field: "+ object.getClass().getName() +"."+ field.getName(), e);
    }
    
    return array;
  }
  
  /**
   * Unmarshall a dictionary collection
   */
  protected Map unmarshallDictionaryCollection(Decoder decoder, Object object, Field field, Type generic, Class base, Property annotation) throws CodingException {
    Class collection = null;
    Class key = null, value = null;
    Map dict;
    
    int count;
    if(decoder.decodeType() == null){
      return (Map)decoder.decodeNull();
    }else{
      count = decoder.decodeDictionary();
    }
    
    Type[] vtypes = (generic instanceof ParameterizedType) ? ((ParameterizedType)generic).getActualTypeArguments() : null;
    
    Class declared;
    if((declared = annotation.dictionary()) != null && !declared.equals(java.lang.Object.class)){
      collection = declared;
    }else if(base.isInterface()){
      collection = HashMap.class;
    }else{
      collection = base;
    }
    
    if((key = annotation.keys()) == null)
      throw new CodingException("Dictionary property declares no concrete key type for unmarshalled object field: "+ object.getClass().getName() +"."+ field.getName());
    if((value = annotation.values()) == null)
      throw new CodingException("Dictionary property declares no concrete value type for unmarshalled object field: "+ object.getClass().getName() +"."+ field.getName());
    
    try {
      dict = (Map)newInstance(collection);
    }catch(Exception e){
      throw new CodingException("Unable to instantiate dictionary of type "+ collection.getName() +"<"+ key.getName() +", "+ value.getName() +"> for unmarshalled field: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    Class<? extends Serializer> keySerializer = annotation.keySerializer();
    Class<? extends Serializer> valueSerializer = annotation.valueSerializer();
    
    for(int i = 0; i < count; i++){
      Object k, v;
      
      if(keySerializer != null && keySerializer != DefaultSerializer.class){
        k = decode(value, decoder, keySerializer);
      }else{
        k = unmarshallValue(decoder, object, key, (vtypes != null && vtypes.length > 0) ? vtypes[0] : null, field, annotation);
      }
      
      if(valueSerializer != null && valueSerializer != DefaultSerializer.class){
        v = decode(value, decoder, valueSerializer);
      }else{
        v = unmarshallValue(decoder, object, value, (vtypes != null && vtypes.length > 1) ? vtypes[1] : null, field, annotation);
      }
      
      dict.put(k, v);
    }
    
    return dict;
  }
  
  /**
   * Encode a value
   */
  protected Encoder encode(Object value, Encoder encoder, Class<? extends Serializer> serializer) throws CodingException {
    return encode(value, encoder, getSerializerInstance(serializer));
  }
  
  /**
   * Encode a value
   */
  protected Encoder encode(Object value, Encoder encoder, Serializer serializer) throws CodingException {
    
    try {
      if(serializer == null){
        encoder.encode(value);
      }else{
        serializer.encode(encoder, value);
      }
    }catch(CodingException e){
      throw e;
    }catch(Exception e){
      throw new CodingException("Unable to encode value", e);
    }
    
    return encoder;
  }
  
  /**
   * Decode a value
   */
  protected Object decode(Class clazz, Decoder decoder, Class<? extends Serializer> serializer) throws CodingException {
    return decode(clazz, decoder, getSerializerInstance(serializer));
  }
  
  /**
   * Decode a value
   */
  protected Object decode(Class clazz, Decoder decoder, Serializer serializer) throws CodingException {
    
    try {
      if(serializer == null){
        return coerce(clazz, decoder.decode());
      }else{
        return coerce(clazz, serializer.decode(decoder));
      }
    }catch(CodingException e){
      if(serializer == null || DefaultSerializer.class.isAssignableFrom(serializer.getClass())){
        throw e;
      }else{
        throw new CodingException("Unable to decode value via serializer: "+ serializer.getClass(), e);
      }
    }catch(Exception e){
      throw new CodingException("Unable to decode value", e);
    }
    
  }
  
  /**
   * Convert a value
   */
  protected Object coerce(Class to, Object value) throws CodingException {
    
    // if the value is null, just return null
    if(value == null) return null;
    // our source type
    Class from = value.getClass();
    // if the types are identical or assignable, just return the value
    if(to.isAssignableFrom(from)) return value;
    
    // convert numeric types
    if(Number.class.isAssignableFrom(from)){
      if(Byte.class.isAssignableFrom(to) || byte.class.isAssignableFrom(to)){
        return new Byte(((Number)value).byteValue());
      }else if(Short.class.isAssignableFrom(to) || short.class.isAssignableFrom(to)){
        return new Short(((Number)value).shortValue());
      }else if(Integer.class.isAssignableFrom(to) || int.class.isAssignableFrom(to)){
        return new Integer(((Number)value).intValue());
      }else if(Long.class.isAssignableFrom(to) || long.class.isAssignableFrom(to)){
        return new Long(((Number)value).longValue());
      }else if(Float.class.isAssignableFrom(to) || float.class.isAssignableFrom(to)){
        return new Float(((Number)value).floatValue());
      }else if(Double.class.isAssignableFrom(to) || double.class.isAssignableFrom(to)){
        return new Double(((Number)value).doubleValue());
      }
    }else if(Boolean.class.isAssignableFrom(from)){
      return (Boolean)value;
    }
    
    // other types are not convertable; so if we're at this point conversion has failed
    throw new CodingException("Unable to convert type: "+ from.getName() +" to type: "+ to.getName() +" for property.");
    
  }
  
  /**
   * Instantiate an object
   */
  protected <T> T newInstance(Class<T> clazz) throws CodingException {
    T object = null;
    
    try {
      Constructor<T> constructor = clazz.getDeclaredConstructor(/* no parameters */);
      boolean accessible = constructor.isAccessible();
      if(!accessible) constructor.setAccessible(true);
      object = constructor.newInstance();
      if(accessible != constructor.isAccessible()) constructor.setAccessible(accessible);
    }catch(Exception e){
      throw new CodingException("Unable to instantiate instance of type: "+ clazz.getName(), e);
    }
    
    return object;
  }
  
  /**
   * Obtain the base type from either a Class or ParameterizedType
   */
  protected Class getBaseType(Type type) {
    if(type instanceof Class){
      return (Class)type;
    }else if(type instanceof ParameterizedType){
      return getBaseType(((ParameterizedType)type).getRawType());
    }else{
      return null;
    }
  }
  
  /**
   * Validate an object.
   */
  public void validate(Object object) throws CodingException {
    try {
      validateObject(object);
    }catch(CodingException e){
      throw e;
    }catch(Exception e){
      throw new CodingException("Unable to validate object of type: "+ object.getClass().getName(), e);
    }
  }
  
  /**
   * Validate an object.
   */
  protected void validateObject(Object object) throws CodingException {
    if(Coding.class.isAssignableFrom(object.getClass())){
      validateObjectProperties(object, getPropertiesForObject(object));
    }else{
      throw new CodingException("Validated objects must implement "+ Coding.class.getName() +": "+ object.getClass().getName());
    }
  }
  
  /**
   * Validate object properties.
   */
  protected void validateObjectProperties(Object object, List<Field> fields) throws CodingException {
    for(Field field : fields) validateObjectProperty(object, field);
  }
  
  /**
   * Validate object properties.
   */
  protected void validateObjectProperty(Object object, Field field) throws CodingException {
    boolean accessible;
    Object value;
    
    try {
      if(!(accessible = field.isAccessible())) field.setAccessible(true);
    }catch(SecurityException e){
      throw new CodingException("Unable to make field of validated object accessible: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    Property annotation;
    if((annotation = field.getAnnotation(Property.class)) == null){
      throw new CodingException("No property annotation found on validated object field: "+ object.getClass().getName() +"."+ field.getName());
    }
    
    try {
      value = field.get(object);
    }catch(Exception e){
      throw new CodingException("Validated object field could not be accessed: "+ object.getClass().getName() +"."+ field.getName(), e);
    }
    
    Class[] validators;
    if((validators = annotation.validators()) != null && validators.length > 0){
      for(int i = 0; i < validators.length; i++){
        getValidatorInstance(validators[i]).validate(object, field, value);
      }
    }
    
    if(value != null && Coding.class.isAssignableFrom(value.getClass())){
      validateObject(value);
    }
    
    try {
      if(field.isAccessible() != accessible) field.setAccessible(accessible);
    }catch(SecurityException e){
      throw new CodingException("Unable to restore field accessibility of validated object: "+ object.getClass().getName() +"."+ field.getName());
    }
    
  }
  
  /**
   * Obtain an ordered list of serializable properties for the provided object
   */
  public List<Field> getPropertiesForObject(Object object) throws CodingException {
    return getPropertiesForClass(object.getClass());
  }
  
  /**
   * Obtain an ordered list of serializable properties for the provided class. We iterate over
   * the class hierarchy from java.lang.Object down to object.getClass() and process properties.
   * The properties of each class have their own index space to keep hierarchies managable. therefore,
   * a property at index 0 in java.langObject (if one existed) would be at index 0 in the message;
   * a property at index 0 in object.getClass() would be at index 1 in the message, assuming
   * object.getClass() inherits directly from java.lang.Object.
   */
  public List<Field> getPropertiesForClass(Class clazz) throws CodingException {
    LinkedList<Field> properties = new LinkedList<Field>();
    for(Class current = clazz; current != null; current = current.getSuperclass()) properties.addAll(0, getDeclaredPropertiesForClass(current));
    return properties;
  }
  
  /**
   * Obtain an ordered list of serializable properties for the provided class
   */
  protected List<Field> getDeclaredPropertiesForClass(Class clazz) throws CodingException {
    Field[] fields;
    Field[] ordered;
    
    try {
      fields = clazz.getDeclaredFields();
      ordered = new Field[fields.length];
    }catch(SecurityException e){
      throw new CodingException("Unable to introspect class: "+ clazz.getName(), e);
    }
    
    Set<Field> unordered = new HashSet<Field>();
    for(int i = 0; i < fields.length; i++) unordered.add(fields[i]);
    
    int count = 0;
    for(Field field : unordered){
      Property annotation;
      if((annotation = field.getAnnotation(Property.class)) != null){
        fields[count++] = field;
      }
    }
    
    for(int i = 0; i < count; i++){
      Field field = fields[i];
      Property annotation;
      if((annotation = field.getAnnotation(Property.class)) != null){
        int index = annotation.index();
        if(index < 0) throw new CodingException("Marshalled object property index must not be negative; found index: "+ index +" on "+ clazz.getName() +"."+ field.getName() +". Did you forget to set the index property?");
        if(index >= count) throw new CodingException("Marshalled object property index must be less than the total number of marshalled properties; found index: "+ index +" on "+ clazz.getName() +"."+ field.getName() +", which is greater than the property count: "+ count +". Do you have a gap in your indices?");
        if(ordered[index] != null) throw new CodingException("Marshalled object property index must be unique; found index: "+ index +" on more than one property of "+ clazz.getName());
        ordered[index] = field;
      }
    }
    
    List<Field> serializable = new ArrayList<Field>();
    for(int i = 0; i < ordered.length; i++) if(ordered[i] != null){ serializable.add(ordered[i]); }
    
    return serializable;
  }
  
}


