// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.validator;

import java.util.Map;
import java.util.Collection;

import java.lang.reflect.Array;
import java.lang.reflect.Field;

import com.brianwolter.coding.Validator;
import com.brianwolter.coding.CodingException;

/**
 * A not-empty property validator asserts that an object is not "empty". This validator
 * can be applied to the following types: String, Array, Collection, Map. The meaning
 * of "empty" varies between types, but should be self explanatory. Not-empty implies
 * not-null.
 */
public class NotEmptyPropertyValidator implements Validator {
  
  /**
   * Validate a decoded object
   */
  public void validate(Object object, Field field, Object value) throws CodingException {
    if(value == null) throw new CodingException("Property must not be null: "+ object.getClass().getName() +"."+ field.getName());
    if(value.getClass().isArray()){
      if(Array.getLength(value) < 1) throw new CodingException("Array value must not be empty: "+ object.getClass().getName() +"."+ field.getName());
    }else if(String.class.isAssignableFrom(value.getClass())){
      if(((String)value).length() < 1) throw new CodingException("String value must not be empty: "+ object.getClass().getName() +"."+ field.getName());
    }else if(Collection.class.isAssignableFrom(value.getClass())){
      if(((Collection)value).size() < 1) throw new CodingException("Collection value must not be empty: "+ object.getClass().getName() +"."+ field.getName());
    }else if(Map.class.isAssignableFrom(value.getClass())){
      if(((Map)value).size() < 1) throw new CodingException("Map value must not be empty: "+ object.getClass().getName() +"."+ field.getName());
    }else{
      throw new CodingException("Not-empty validator cannot be applied to "+ value.getClass().getName() +" in: "+ object.getClass().getName() +"."+ field.getName());
    }
  }
  
}



