// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.validator;

import java.lang.reflect.Field;

import com.brianwolter.coding.*;

/**
 * String length property validator
 */
public class StringLengthPropertyValidator implements Validator {
  
  private int _minimum;
  private int _maximum;
  
  /**
   * Construct with bounds
   */
  public StringLengthPropertyValidator(int minimum, int maximum) {
    _minimum = minimum;
    _maximum = maximum;
  }
  
  /**
   * Validate a decoded object
   */
  public void validate(Object object, Field field, Object value) throws CodingException {
    if(value == null) throw new CodingException("Property must not be null: "+ object.getClass().getName() +"."+ field.getName());
    if(!String.class.isAssignableFrom(value.getClass())) throw new CodingException("Property must be a string: "+ object.getClass().getName() +"."+ field.getName());
    int length = ((String)value).length();
    if(_minimum > 0 && length < _minimum) throw new CodingException("Property string is too short: "+ object.getClass().getName() +"."+ field.getName() +" (must be at least "+ _minimum +" characters)");
    if(_maximum > 0 && length > _maximum) throw new CodingException("Property string is too long: "+ object.getClass().getName() +"."+ field.getName() +" (must be at most "+ _maximum +" characters)");
  }
  
}



