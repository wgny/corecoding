// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding;

import java.nio.ByteBuffer;

/**
 * Implemented by serializing encoders.
 */
public interface Encoder {
  
  /**
   * Determine if the provided object is a supported property type.
   */
  public boolean isValueSupported(Object value);
  
  /**
   * Determine if the provided class is a supported property type.
   */
  public boolean isTypeSupported(Class clazz);
  
  /**
   * Obtain encoded bytes
   */
  public byte[] bytes();
  
  /**
   * Obtain encoded byte buffer
   */
  public ByteBuffer buffer();
  
  /**
   * Encode an object. The object must be a supported property type.
   */
  public void encode(Object value) throws CodingException;
  
  /**
   * Encode a null value
   */
  public void encodeNull();
  
  /**
   * Encode a boolean value
   */
  public void encodeBoolean(boolean value);
  
  /**
   * Encode a integer value
   */
  public void encodeInteger(long value);
  
  /**
   * Encode a double value
   */
  public void encodeDouble(double value);
  
  /**
   * Encode a string value
   */
  public void encodeString(String value);
  
  /**
   * Encode a byte array value
   */
  public void encodeData(byte[] bytes);
  
  /**
   * Encode an array value
   */
  public void encodeArray(int count);
  
  /**
   * Encode an dictionary value
   */
  public void encodeDictionary(int count);
  
}


