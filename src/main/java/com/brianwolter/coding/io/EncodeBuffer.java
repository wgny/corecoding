// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.io;

import java.io.IOException;
import java.io.OutputStream;

import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.nio.channels.ClosedChannelException;

import java.util.List;
import java.util.ArrayList;

import com.brianwolter.coding.*;
import com.brianwolter.coding.marshall.*;

/**
 * An encode buffer accumulates messages, encodes them, and buffers the encoded data
 * so that it may be written out incrementally to a channel or stream. Encode buffers
 * implement a simple framing mechanism to deliniate individual, distinct messages.
 */
public class EncodeBuffer<M> {
  
  private static final int RESIZE_FACTOR = 2;
  
  private final int           _capacity;
  private ObjectMarshaller<M> _marshaller;
  private ByteBuffer          _buffer;
  
  /**
   * Construct an accumulator
   */
  public EncodeBuffer(ObjectMarshaller<M> marshaller) {
    this(marshaller, 2048, 65536);
  }
  
  /**
   * Construct an accumulator with an initial capacity
   */
  public EncodeBuffer(ObjectMarshaller<M> marshaller, int initialCapacity, int limitCapacity) {
    _marshaller = marshaller;
    _buffer = ByteBuffer.allocate(initialCapacity);
    _capacity = limitCapacity; // "soft" capacity
  }
  
  /**
   * Write encoded data to the channel.
   */
  public synchronized int write(WritableByteChannel channel) throws IOException, CodingException {
    int z = 0;
    if(_buffer.position() > 0){
      // flip the buffer for writing
      _buffer.flip();
      // write as much data to the channel as we're able
      if((z = channel.write(_buffer)) < 0) throw new ClosedChannelException();
      // update the buffer to make room for more data
      _buffer.compact().limit(_buffer.capacity());
    }
    return z;
  }
  
  /**
   * Write encoded data to the stream.
   */
  public synchronized int write(OutputStream stream) throws IOException, CodingException {
    int z = 0;
    if(_buffer.position() > 0){
      // flip the buffer for writing
      _buffer.flip();
      // write as much data to the channel as we're able
      stream.write(_buffer.array(), 0, _buffer.limit());
      // flush the stream
      stream.flush();
      // update the buffer to make room for more data
      _buffer.position(0).limit(_buffer.capacity());
    }
    return z;
  }
  
  /**
   * Append an encoder to this buffer
   */
  public synchronized void append(M message) throws IOException, CodingException {
    append(_marshaller.marshall(message));
  }
  
  /**
   * Append an encoder to this buffer
   */
  public synchronized void append(ByteBuffer buffer) throws IOException, CodingException {
    try {
      int sizeof_int = Integer.SIZE / 8;
      // require capacity in our internal buffer for the message
      requireCapacity(_buffer.position() + buffer.remaining() + sizeof_int);
      // make sure the limit is at the buffer's capacity
      _buffer.limit(_buffer.capacity());
      // append the message length to our buffer
      _buffer.putInt(buffer.remaining());
      // append the message to our buffer
      _buffer.put(buffer);
    }catch(java.nio.BufferOverflowException e){
      throw new CodingException("Buffer overflow while appending "+ buffer.remaining() +" bytes to: "+ _buffer, e);
    }catch(java.nio.BufferUnderflowException e){
      throw new CodingException("Buffer underflow while appending "+ buffer.remaining() +" bytes to: "+ _buffer, e);
    }
  }
  
  /**
   * Obtain the number of bytes remaining in the buffer waiting to be written. This method should
   * not be invoked during a write operation.
   */
  public synchronized int remaining() {
    return _buffer.position();
  }
  
  /**
   * Determine if we have capacity for more data in this buffer. This is a soft limit which is intended
   * to inform clients when the buffer becomes exceedingly large.
   */
  public synchronized boolean hasCapacity() {
    return remaining() < 1 || (remaining() < (_capacity / 2));
  }
  
  /**
   * Require capacity in our buffer. The buffer is reallocated as necessary.
   * 
   * @return whether or not the buffer was reallocated to account for the required capacity
   */
  protected synchronized boolean requireCapacity(int capacity) {
    if(_buffer.capacity() < capacity){
      capacity = Math.max(capacity, _buffer.capacity() * RESIZE_FACTOR);
      int position = _buffer.position();
      _buffer = ByteBuffer.allocate(capacity).put((ByteBuffer)_buffer.flip());
      _buffer.position(position).limit(_buffer.capacity());
      return true;
    }else{
      return false;
    }
  }
  
}


