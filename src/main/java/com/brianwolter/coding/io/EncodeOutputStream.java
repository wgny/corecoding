// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.io;

import java.io.*;

import com.brianwolter.coding.*;
import com.brianwolter.coding.message.*;

/**
 * A input stream which decodes messages
 */
public class EncodeOutputStream<M> extends OutputStream {
  
  private OutputStream      _ostream;
  private EncodeBuffer<M>   _encoder;
  
  /**
   * Construct
   */
  public EncodeOutputStream(OutputStream ostream, EncodeBuffer<M> encoder) {
    _ostream = ostream;
    _encoder = encoder;
  }
  
  /**
   * Write a single byte to the underlying output stream
   */
  public void write(int b) throws IOException {
    throw new IOException("This method is not supported by "+ this.getClass().getName());
  }
  
  /**
   * Write a message to the underlying output  stream
   */
  public void writeMessage(M message) throws IOException, CodingException {
    // append our message
    _encoder.append(message);
    // write our request
    _encoder.write(_ostream);
  }
  
}

