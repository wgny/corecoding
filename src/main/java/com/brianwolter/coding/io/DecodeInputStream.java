// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.io;

import java.io.*;
import java.util.*;
import java.nio.ByteBuffer;

import com.brianwolter.coding.*;
import com.brianwolter.coding.message.*;

/**
 * A input stream which decodes messages
 */
public class DecodeInputStream<M> extends InputStream {
  
  private InputStream       _istream;
  private DecodeBuffer<M>   _decoder;
  private Queue<M>          _messages;
  
  private int               _length;
  private byte[]            _chunk;
  private ByteBuffer        _buffer;
  
  /**
   * Construct
   */
  public DecodeInputStream(InputStream istream, DecodeBuffer<M> decoder) {
    _istream = istream;
    _decoder = decoder;
    _messages = new LinkedList<M>();
    _length = 2048;
    _chunk = new byte[_length];
    _buffer = ByteBuffer.allocate(_length);
  }
  
  /**
   * Read a byte from the underlying input stream
   */
  public int read() throws IOException {
    throw new IOException("This method is not supported by "+ this.getClass().getName());
  }
  
  /**
   * Read a message, expecting a particular type
   */
  public <C extends M> C readMessage(Class<? extends C> type) throws IOException, CodingException {
    
    M message;
    if((message = readMessage()) != null){
      if(type.isAssignableFrom(message.getClass())){
        return (C)message;
      }else{
        throw new CodingException("The stream did not produce the expected message type (expected: "+ type.getName() +"; received: "+ message.getClass().getName() +")");
      }
    }
    
    throw new EOFException("Input stream ended");
  }
  
  /**
   * Read a message
   */
  public M readMessage() throws IOException, CodingException {
    
    M message;
    if((message = _messages.poll()) != null){
      return message;
    }
    
    int z;
    while((z = _istream.read(_chunk)) > 0){
      Collection<M> messages;
      // attempt to decode messages from our buffer; if we get some, return the first one
      if((messages = _decoder.append((ByteBuffer)_buffer.put(_chunk, 0, z).flip())) != null && messages.size() > 0) _messages.addAll(messages);
      // clear the buffer for our next read
      _buffer.clear();
      // if we produced a message, return it
      if(_messages.size() > 0) return _messages.poll();
    }
    
    throw new EOFException("Input stream ended");
  }
  
}

