// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.io;

import java.io.IOException;
import java.io.EOFException;
import java.io.InputStream;

import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.ClosedChannelException;

import java.util.List;
import java.util.ArrayList;

import com.brianwolter.coding.*;
import com.brianwolter.coding.marshall.*;

/**
 * A decode buffer accumulates data incrementally from a channel or stream until there is
 * enough to produce an encoded message. Decode buffers implement a simple framing mechanism
 * to deliniate individual, distinct messages.
 */
public class DecodeBuffer<M> {
  
  private static final int RESIZE_FACTOR = 2;
  
  private ObjectMarshaller<M> _marshaller;
  private ByteBuffer          _buffer;
  private ByteBuffer          _chunk;
  private byte[]              _bytes;
  
  /**
   * Construct an accumulator
   */
  public DecodeBuffer(ObjectMarshaller<M> marshaller) {
    this(marshaller, 2048);
  }
  
  /**
   * Construct an accumulator with an initial capacity
   */
  public DecodeBuffer(ObjectMarshaller<M> marshaller, int initialCapacity) {
    _marshaller = marshaller;
    _buffer = ByteBuffer.allocate(initialCapacity);
  }
  
  /**
   * Read from the channel and produce message decoders encapsulating encoded messages.
   * If null is returned, not enough data has been read to produce a full message.
   */
  public synchronized List<M> read(ReadableByteChannel channel) throws IOException, CodingException {
    int z;
    // make sure we have a chunk buffer
    if(_chunk == null) _chunk = ByteBuffer.allocate(2048);
    else _chunk.rewind(); // if we already have one, rewind it
    // attempt to read some bytes and make sure all is well
    if((z = channel.read(_chunk)) < 0) throw new ClosedChannelException();
    // append to the accumulator if we got some bytes
    return (z > 0) ? append((ByteBuffer)_chunk.flip()) : null;
  }
  
  /**
   * Read from the channel and produce message decoders encapsulating encoded messages.
   * If null is returned, not enough data has been read to produce a full message.
   */
  public synchronized List<M> read(InputStream stream) throws IOException, CodingException {
    int z;
    // make sure we have a chunk buffer
    if(_bytes == null) _bytes = new byte[2048];
    // attempt to read some bytes and make sure all is well
    if((z = stream.read(_bytes)) < 0) throw new EOFException();
    // append to the accumulator if we got some bytes
    return (z > 0) ? append(ByteBuffer.wrap(_bytes, 0, z)) : null;
  }
  
  /**
   * Append data to this accumulator and produce message decoders encapsulating encoded
   * messages. If null is returned, not enough data has been read to produce a full message.
   */
  public synchronized List<M> append(ByteBuffer buffer) throws IOException, CodingException {
    List<M> messages = null;
    int sizeof_int = (Integer.SIZE / 8);
    int length;
    
    // make sure the buffer is valid
    if(buffer == null) throw new IllegalArgumentException("Buffer is null");
    // make sure we have capacity for the provided buffer
    requireCapacity(_buffer.position() + buffer.remaining());
    // append the provided buffer to our internal buffer
    _buffer.put(buffer);
    
    // while we have a pending count and we've buffered enough it, process a message
    while(_buffer.position() >= sizeof_int && (_buffer.position() - sizeof_int) >= (length = _buffer.getInt(0))){
      if(length > 0){
        if(messages == null) messages = new ArrayList<M>();
        messages.add(decode((ByteBuffer)_buffer.flip()));
      }else{
        _buffer.getInt();
        _buffer.compact();
      }
    }
    
    // return decoded messages
    return messages;
  }
  
  /**
   * Require capacity in our buffer. The buffer is reallocated as necessary.
   */
  protected synchronized void requireCapacity(int capacity) {
    if(_buffer.capacity() < capacity){
      capacity = Math.max(capacity, _buffer.capacity() * RESIZE_FACTOR);
      int position = _buffer.position();
      _buffer = ByteBuffer.allocate(capacity).put((ByteBuffer)_buffer.flip());
      _buffer.position(position);
    }
  }
  
  /**
   * Decode a message buffer
   */
  protected synchronized M decode(ByteBuffer buffer) throws CodingException {
    // obtain the messsage length (relative)
    int length = buffer.getInt();
    // make sure the entire message is present in the buffer
    if(buffer.remaining() < length) throw new IllegalStateException("Buffer does not contain enough bytes to deserialize message: "+ length +" required; "+ buffer.remaining() +" available.");
    // deserialize the message
    M message = _marshaller.unmarshall(buffer);
    // update the parameter buffer to cue up the next message
    buffer.compact();
    // return our decoder
    return message;
  }
  
}


