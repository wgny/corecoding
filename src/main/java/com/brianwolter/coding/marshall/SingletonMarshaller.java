// 
// Copyright (c) 2012 Wolter Group New York, Inc. All rights reserved.
// 
// @WOLTER_GROUP_LICENSE_START@
// 
// This software is provided to you by Wolter Group New York ('Wolter')
// in consideration of your acceptance of the terms under which it was
// licensed to you (the 'License'). You may not use this software except
// in compliance with the License.
// 
// Please refer to the License that you received from Wolter or to the
// relevant sections of the agreement under which the larger project
// incorporating this software was developed for you by Wolter or a
// licensee of Wolter.
// 
// This software is distributed on an 'AS IS' basis, WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, AND WOLTER HEREBY DISCLAIMS ALL
// SUCH WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
// NON-INFRINGEMENT.
// 
// @WOLTER_GROUP_LICENSE_END@
// 
// Developed by Wolter Group in New York City
// 

package com.brianwolter.coding.marshall;

import java.nio.ByteBuffer;

import com.brianwolter.coding.Marshaller;
import com.brianwolter.coding.CodingException;
import com.brianwolter.coding.message.MessageEncoder;
import com.brianwolter.coding.message.MessageDecoder;

/**
 * A marshaller that marshalls to and from a single, defined type.
 */
public class SingletonMarshaller<T> extends ObjectMarshaller<T> {
  
  private Class<T>    _clazz;
  
  /**
   * Construct with a type to marshall to and from
   */
  public SingletonMarshaller(Class<T> clazz) {
    _clazz = clazz;
  }
  
  /**
   * Marshall an object into an encoded message.
   */
  public ByteBuffer marshall(T object) throws CodingException {
    return marshall(object, new MessageEncoder()).buffer();
  }
  
  /**
   * Unmarshall an encoded message into an object.
   */
  public T unmarshall(ByteBuffer buffer) throws CodingException {
    return unmarshall(new MessageDecoder(buffer), _clazz);
  }
  
}


